require 'uuidtools'
require 'base64'
class Order < ActiveRecord::Base
  attr_accessible :also, :menu, :what, :whenby, :currency
  attr_accessible :name
  
  validates :what, :presence => true  
  validates :currency, :presence => true
  validates :name, :presence => true
  
  def name
    @name
  end
  
  def name= name
    @name = name
  end
  
  def to_param
	   "#{self.key}"
  end
  before_create do
	  self.key = UUIDTools::UUID.random_create.to_s
  end
  
  has_many :orderPeople, :dependent => :destroy
  has_many :orderItems, :through => :orderPeople
  
  after_initialize :set_defaults
  private
  def set_defaults
    self.currency = "$" if self.currency == nil and self.new_record?
  end 
  
  
end
