class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :what
      t.string :whenby
      t.string :menu
      t.text :also
      t.string :key
      t.string :currency

      t.timestamps
    end
  end
end
