class OrderItem < ActiveRecord::Base
  belongs_to :orderperson
  attr_accessible :desc, :price, :qty
  validates :desc, :presence => true
  validates :price, :presence => true, :numericality => true 
  validates :qty, :presence => true, :numericality => { :only_integer => true }

  validates_length_of :desc, :maximum => 100

  before_create do
	  self.key = UUIDTools::UUID.random_create.to_s
  end
  
  after_initialize :set_defaults
  private
  def set_defaults
    self.qty = 1 if self.qty == nil and self.new_record?
  end 
end
