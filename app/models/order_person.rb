class OrderPerson < ActiveRecord::Base
  belongs_to :order
  attr_accessible :name
  has_many :orderItems, :dependent => :destroy
  validates :name, :presence => true
end
