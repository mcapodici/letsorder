require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
	assert_select 'title', "Let&#x27;s Order - Get Friends Together And Order Takeaway"
	assert_select "form textarea" do
	  assert_select "[name='order[also]']" 
	end
	assert_select "form input" do
	  assert_select "[name='order[name]']"
	  assert_select "[name='order[what]']"
	  assert_select "[name='order[whenby]']"
	  assert_select "[name='order[menu]']"
	  assert_select "[name='order[currency]']"
	end
  end
end
