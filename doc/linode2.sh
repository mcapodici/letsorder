curl -L get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
apt-get update
#rvm requirements
apt-get -y --no-install-recommends install build-essential openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev libgdbm-dev ncurses-dev automake libtool bison subversion pkg-config libffi-dev
rvm install 1.9.3
rvm use 1.9.3 --default
rvm rubygems current
gem install rails
gem install passenger 
apt-get -y install libcurl4-openssl-dev
passenger-install-nginx-module
#...option 1
# Download nginx startup script
wget -O init-deb.sh http://library.linode.com/assets/660-init-deb.sh
# Move to the init.d directory and make executable
mv init-deb.sh /etc/init.d/nginx
chmod +x /etc/init.d/nginx
# Add nginx to the system startup
/usr/sbin/update-rc.d -f nginx defaults
service nginx start
apt-get -y install mysql-server
apt-get -y install libmysqlclient-dev libmysqlclient18 ruby-dev
#mysqladmin -h localhost -u root -ppassword create letsorder
#mysql -h localhost -u root -ppassword
#CREATE USER 'letsorder_user'@'localhost' IDENTIFIED BY 'some_pass';
#grant usage on *.* to letsorder_user@localhost;
#grant all privileges on letsorder.* to letsorder_user@localhost ;
#mysql -u letsorder_user -p''


: <<'END'
    server {
        listen 80;
        server_name localhost;
        root /var/www/racktest/public;
        passenger_enabled on;
    }
END

#useful commands:
#vi /opt/nginx/conf/nginx.conf
#bundle exec rake assets:precompile
