class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.string :originaldesc
      t.string :desc
      t.decimal :price, :precision => 19, :scale => 4
      t.integer :qty
      t.references :order_person
      t.string :key
      t.timestamps
    end
    add_index :order_items, :order_person_id
  end
end
