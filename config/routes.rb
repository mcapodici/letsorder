Letsorder::Application.routes.draw do
  root :to => "home#index"
  get "home/index"

 # resources :orders 

  match 'orders/tell/:key', :to  => 'orders#tell', :via => :get
  match 'orders/addname/:key', :to  => 'orders#newperson', :via => :get 
  match 'orders/addname/:key', :to  => 'orders#createperson', :via => :post 
  match 'orders', :to  => 'orders#create', :via => :post 
  match 'orders/:key', :to  => 'orders#show', :via => :get , :as  =>  :order
  match 'orders/additem/:key', :to  => 'orders#createitem', :via => :post
  match 'orders/additem/:key', :to => redirect('/orders/%{key}'), :via => :get
  match 'orders/addtomine/:key', :to => 'orders#copyitem', :via => :get 
  match 'orders/removeitem/:key', :to => 'orders#removeitem', :via => :get 
  match 'orders/manageall/:key', :to => 'orders#manageall', :via => :get

   
   
   
     #order GET    /orders/:id(.:format)          orders#show
#  match 'orders-people/:id-:key', :to => 'order_person#show', :as => :Orders
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically): 
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
