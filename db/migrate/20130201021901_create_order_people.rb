class CreateOrderPeople < ActiveRecord::Migration
  def change
    create_table :order_people do |t|
      t.string :name
      t.references :order

      t.timestamps
    end
    add_index :order_people, :order_id
	add_index :order_people, [:name, :order_id], :unique => true
  end
end
