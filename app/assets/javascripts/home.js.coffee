# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ -> 
  defaultForm()

timesuggest = (h) ->
  return "Noon" if h <= 10
  return "6 o'clock" if h <= 16
  return "10 o'clock" if h <= 20
  return "Midnight" if h <= 22
  "3 am"
  
defaultForm = -> 
  d = new Date()
  h = d.getHours()
  $('#order_whenby').watermark('e.g. ' + timesuggest(h))
  $('#order_what').watermark('e.g. Chinese from Szechuan Garden')
  $('#order_menu').watermark('e.g. On my desk')
