class ApplicationController < ActionController::Base
  protect_from_forgery
end

def not_found
  raise ActionController::RoutingError.new('Not Found')
end

def itemsTotalForPerson(orderItems)  
  return orderItems.sum { |p| p.qty * p.price }
end