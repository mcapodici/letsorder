class OrdersController < ApplicationController
  before_filter  :order_controller_common

  def new
    @order = Order.new

    respond_to do |format|
      format.html
    end
  end

  def create
    @order = Order.new(params[:order])

    respond_to do |format|
      if @order.save

        @orderPerson = @order.orderPeople.build
        @orderPerson.name = @order.name
        @orderPerson.save!
        session[:order_person_id] = @orderPerson.id
		session[:tellfriends] = true;
        format.html { redirect_to :action => "show", :key => @order.key }
      else
        format.html { render :template => "home/index" }
      end
    end
  end

  def show
	if params[:hidetellfriends] == 'y' && session[:tellfriends] == true
		session[:tellfriends] = false;		
	end
    if @orderPerson == nil
      @orderPerson = @order.orderPeople.build
    else
	  @orderItem = OrderItem.new
	end 
  end

  def newitem

  end

  def createitem
    @orderItem = add_order_item(
		params[:order_item][:desc], 
		params[:order_item][:price].to_f,
		params[:order_item][:qty].to_i)

    if @orderItem.valid? then
      @orderItem.save!
      respond_to do |format|
        format.html { redirect_to :action => "show", :key => @order.key }
      end
    else
      respond_to do |format|
        format.html { render :action => "show" }
      end
    end

  end

  def newperson
    @orderPerson = @order.orderPeople.build
    respond_to do |format|
      format.html { render action: "newperson" }
    end
  end

  def createperson
    @orderPerson = @order.orderPeople.build(params[:order_person])

    if @orderPerson.save then
      session[:order_person_id] = @orderPerson.id
      redirect_to order_path(@order)
    else
      respond_to do |format|
        format.html { render action: "newperson" }
      end
    end
  end

  def copyitem
    orderItemToCopy = OrderItem.find_by_key(params[:orderitemkey])
    add_order_item(orderItemToCopy.desc, orderItemToCopy.price, 1).save!
    respond_to do |format|
      format.html { redirect_to :action => "show", :key => @order.key }
    end
  end
  
  def removeitem
	raise "error" if not @orderPerson
    orderItemToRemove = OrderItem.find(params[:orderitemid])
	raise "error" if not orderItemToRemove.order_person_id == @orderPerson.id
	
    orderItemToRemove.destroy
    respond_to do |format|
      format.html { redirect_to :action => "show", :key => @order.key }
    end
  end

  def manageall
  end

  private
  
	class TeamOrderItem 
		attr_reader :desc
		attr_reader :price
		attr_reader :qty
		attr_reader :key
	   def initialize(desc, price, qty, key)
		  @desc=desc
		  @price=price
		  @qty=qty
		  @key=key
	   end
	end

  def order_controller_common
    @order = Order.find_by_key(params[:key])
    @orderPerson = get_current_order_person(@order)	if @order
	
	if @order != nil and @order.orderItems.count > 0
		@teamOrderItems = @order.orderItems.group_by{|i|[i.desc,i.price]}.map {|group, items|
			TeamOrderItem.new( group[0], group[1], items.sum(&:qty), items.first.key)
		}		
	end
  end

  def get_current_order_person(order)
    orderPerson = nil
    if session[:order_person_id] then
	  return nil if not OrderPerson.exists?(session[:order_person_id])
      orderPerson = OrderPerson.find(session[:order_person_id])     
      return nil if orderPerson.order_id != order.id
    else
      return nil
    end
    return orderPerson
  end
  
  def add_order_item(desc, price, qty)
    orderItemToUpdate = @orderPerson.orderItems.where(:desc => desc, :price => price).first

    if orderItemToUpdate == nil
		orderItemToUpdate = @orderPerson.orderItems.build()
		orderItemToUpdate.qty = qty
		orderItemToUpdate.desc = desc
		orderItemToUpdate.price = price
		
		if !orderItemToUpdate.valid?
			@orderPerson.orderItems.delete(orderItemToUpdate)
		end
    else
		orderItemToUpdate.qty += qty
	end
	return orderItemToUpdate
  end
end
